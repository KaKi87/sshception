import {
    PassThrough
} from 'node:stream';

export default () => {
    const stdin = new PassThrough();
    return {
        stdin,
        stdout: new PassThrough(),
        setSize: ({ rows, columns }) => {
            stdin.rows = rows;
            stdin.columns = columns;
            stdin.emit('resize');
        }
    };
};