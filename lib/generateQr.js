import qr from 'qrcode-terminal';

export default data => new Promise(resolve => qr.generate(
    data,
    { small: true },
    resolve
));