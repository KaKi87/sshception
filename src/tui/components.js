import blessed from 'blessed';
import contrib from 'blessed-contrib';

const
    keys = true,
    padding = {
        left: 1,
        right: 1
    },
    border = 'line',
    type   = border,
    white  = 'white',
    black  = 'black';

export const
    form = (
        parent,
        { top, width, height, padding, hidden } = {},
        { borders } = {}
    ) => blessed.form({
        parent,
        keys,
        top,
        width: `${width || 100}%`,
        height,
        ...(borders ? {
            border: {
                type,
                fb: white
            }
        } : {}),
        padding,
        hidden
    }),
    text = (
        parent,
        { top, content, hidden } = {},
        {
            color,
            borders,
            borderBottom
        } = {}
    ) => {
        const _text = blessed.text({
            parent,
            top,
            content: borderBottom ? `${content}\n${content.replace(/./g, '_')}` : content,
            ...(color ? {
                style: {
                    fg: color
                }
            } : {}),
            ...(borders ? {
                border: {
                    type,
                    fg: color
                },
                padding
            } : {}),
            hidden
        });
        _text.$height = content ? content.split('\n').length : undefined;
        return _text;
    },
    button = (
        parent,
        { top, content, hidden },
        { color } = {}
    ) => blessed.button({
        parent,
        top,
        content,
        border: {
            type,
            fg: color
        },
        padding,
        style: {
            fg: color,
            focus: color ? {
                fg: white,
                bg: color
            } : {
                fg: black,
                bg: white
            }
        },
        shrink: true,
        hidden
    }),
    textbox = (
        parent,
        { top, content, name, censor }
    ) => {
        const _textbox = blessed.textbox({
            parent,
            top,
            name,
            censor,
            inputOnFocus: true,
            border,
            height: 3
        });
        if(content) _textbox.setValue(content.toString());
        _textbox.key('enter', () => _textbox.focus());
        return _textbox;
    },
    checkbox = (
        parent,
        { top, content, name }
    ) => blessed.checkbox({
        parent,
        top,
        content,
        name
    }),
    flexContainer = (
        parent,
        { top, width, height } = {}
    ) => blessed.layout({
        parent,
        top,
        width: `${width || 100}%`,
        height: height || 3
    }),
    table = (
        parent,
        { top, width, height, columnsWidth, headers, data }
    ) => {
        const
            _width = Math.floor(parent.width * width / 100),
            _table = contrib.table({
                parent,
                keys,
                top,
                width: _width,
                height,
                border,
                columnSpacing: 0,
                columnWidth: columnsWidth.map(colW => Math.floor(_width * colW / 100)),
                fg: white,
                selectedFg: black,
                selectedBg: white
            });
        _table.setData({
            headers: headers.map(header => ` ${header}`),
            data
        });
        return _table;
    };