import blessed from 'blessed';
import { createEmitter } from 'simpler-emitter';

import createPassthroughStream from '../../lib/createPassthroughStream.js';
import controllers from './controllers/main.js';

export default async ({
    streamManager: {
        setActiveStream
    },
    username,
    password,
    end
}) => {
    const screenStream = createPassthroughStream();
    setActiveStream(screenStream);
    const
        screen = blessed.screen({
            title: 'SSHception',
            smartCSR: true,
            terminal: 'xterm-256color',
            input: screenStream.stdout,
            output: screenStream.stdin,
            autoPadding: true
        }),
        emitter = createEmitter(),
        _end = () => {
            screen.destroy();
            end();
        },
        user = await controllers.auth(screen)({
            username,
            password,
            end: _end,
            emitter
        });
    screen.on('keypress', (undefined, { name }) => {
        if(name === 'escape')
            emitter.emit('escape');
    });
    controllers.dashboard(screen)({
        user,
        setActiveStream,
        activateScreen: () => {
            setActiveStream(screenStream);
            screen.realloc();
            screen.render();
        },
        end: _end,
        emitter
    });
};