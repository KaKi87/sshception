import { getString } from '../../../strings.js';

import {
    form,
    text,
    textbox,
    flexContainer,
    button
} from '../components.js';
export default (
    screen
) => ({
    isCancellable,
    callback
}) => {
    const otpLoginForm = form(screen);
    text(otpLoginForm, { content: `${getString('loginTotp.title')} :` });
    let n = 2;
    const
        codeInput = textbox(otpLoginForm, { top: n, name: 'code' }),
        actions = flexContainer(otpLoginForm, { top: n += 4 });
    button(actions, { content: getString('cancel'), hidden: !isCancellable })
        .on('press', () => callback({ isCancelled: true }));
    button(actions, { content: getString('submit') })
        .on('press', () => otpLoginForm.submit());
    const errorText = text(otpLoginForm, { top: n + 4, hidden: true }, { color: 'red' });
    otpLoginForm.on('submit', ({ code }) => callback({
        code,
        onError: message => {
            errorText.setContent(getString(message));
            errorText.show();
            switch(message){
                case 'invalidTotpCode': {
                    codeInput.clearValue();
                    otpLoginForm.focusChild(codeInput);
                    break;
                }
            }
        }
    }));
    screen.render();
    codeInput.focus();
};