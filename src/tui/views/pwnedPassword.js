import { getString } from '../../../strings.js';

import {
    text,
    button
} from '../components.js';

export default (
    screen
) => ({
    end
}) => {
    text(
        screen,
        { content: getString('PWNED_PASSWORD') },
        { color: 'red', borders: true }
    );
    const quitButton = button(screen, { top: 5, content: getString('quit') });
    quitButton.on('press', end);
    quitButton.focus();
    screen.render();
};