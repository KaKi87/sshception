import { getString } from '../../../strings.js';

import {
    form,
    text,
    textbox,
    checkbox,
    flexContainer,
    button
} from '../components.js';

export default (
    screen
) => ({
    isTotpEnabled,
    callback
}) => {
    const deleteAccountForm = form(screen);
    text(deleteAccountForm, { content: getString('deleteAccount.title') }, { borderBottom: true });
    let n = 3;
    text(deleteAccountForm, { top: n, content: `${getString('password')} :` });
    const passwordInput = textbox(deleteAccountForm, { top: n += 2, name: 'password', censor: true });
    if(isTotpEnabled){
        text(deleteAccountForm, { top: n += 4, content: `${getString('totp')} :` });
        textbox(deleteAccountForm, { top: n += 2, name: 'totp' });
    }
    checkbox(deleteAccountForm, { top: n += 4, name: 'confirm', content: getString('deleteAccount.confirm') });
    const actions = flexContainer(deleteAccountForm, { top: n += 2 });
    button(actions, { content: getString('cancel') })
        .on('press', () => callback({ isCancelled: true }));
    const submitButton = button(actions, { content: getString('submit') }, { color: 'yellow' })
    submitButton.on('press', () => deleteAccountForm.submit());
    const errorText = text(
        deleteAccountForm,
        { top: n + 4 },
        { color: 'red' }
    );
    deleteAccountForm.on('submit', formData => {
        if(formData.confirm){
            submitButton.setContent({ [getString('submit')]: getString('confirm'), [getString('confirm')]: getString('submit') }[submitButton.content]);
            if(submitButton.content === getString('submit')){
                callback({
                    ...formData,
                    onError: error => {
                        errorText.setContent(getString(error));
                        screen.render();
                    }
                });
            }
        }
        else
            errorText.setContent(getString('deleteAccount.confirmInvalid'));
        screen.render();
    });
    screen.render();
    passwordInput.focus();
};