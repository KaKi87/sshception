import disclaimer from './disclaimer.js';
import wrongPassword from './wrongPassword.js';
import pwnedPassword from './pwnedPassword.js';
import invalidUsername from './invalidUsername.js';
import loginTotp from './loginTotp.js';
import register from './register.js';
import enableTotp from './enableTotp.js';
import home from './home.js';
import setHost from './setHost.js';
import editUserPass from './editUserPass.js';
import deleteAccount from './deleteAccount.js';

export {
    home,
    setHost,
    loginTotp,
    enableTotp,
    editUserPass,
    deleteAccount,
    disclaimer,
    register,
    pwnedPassword,
    invalidUsername,
    wrongPassword
};