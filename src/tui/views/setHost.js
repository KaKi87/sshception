import {
    getString,
    getAnyString
} from '../../../strings.js';

import {
    form,
    text,
    textbox,
    flexContainer,
    button
} from '../components.js';

export default (
    screen
) => ({
    name,
    address,
    port,
    username,
    password,
    chain,
    callback
}) => {
    let action;
    const hostForm = form(screen);
    text(hostForm, { content: getString('addHost') }, { borderBottom: true });
    let n = 3;
    text(hostForm, { content: `${getString('setHost.name')} :`, top: n });
    const nameInput = textbox(hostForm, { top: n += 2, name: 'name', content: name });
    text(hostForm, { top: n += 4, content: `${getString('setHost.address')} :` });
    const addressInput = textbox(hostForm, { top: n += 2, name: 'address', content: address });
    text(hostForm, { top: n += 4, content: `${getString('setHost.port')} :` });
    textbox(hostForm, { top: n += 2, name: 'port', content: port });
    text(hostForm, { top: n += 4, content: `${getString('setHost.username')} :` });
    const userInput = textbox(hostForm, { top: n += 2, name: 'username', content: username });
    text(hostForm, { top: n += 4, content: `${getString('password')} :` });
    textbox(hostForm, { top: n += 2, name: 'password', censor: true, content: password });
    text(hostForm, { top: n += 4, content: `${getString('setHost.chain')} :` });
    textbox(hostForm, { top: n += 2, name: 'chain', content: chain });
    const actions = flexContainer(hostForm, { top: n += 4, width: 100 });
    button(actions, { content: getString('cancel') })
        .on('press', () => callback({ action: 'cancel' }));
    const testButton = button(actions, { content: getString('setHost.test') });
    testButton.on('press', () => {
        action = 'test';
        hostForm.submit();
    });
    button(actions, { content: getString('submit') }, { color: 'green' }).on('press', () => {
        action = 'submit';
        hostForm.submit();
    });
    const
        successText = text(
            hostForm,
            { top: n += 4, content: getString('setHost.testSuccess'), hidden: true },
            { color: 'green' }
        ),
        errorText = text(
            hostForm,
            { top: n, hidden: true },
            { color: 'red' }
        );
    hostForm.on('submit', ({ name, address, port, username, password, chain }) => {
        port = parseInt(port);
        successText.hide();
        errorText.hide();
        if(action === 'test')
            testButton.setContent(getString('setHost.testing'));
        screen.render();
        callback({
            action,
            host: { name, address, port, username, password, chain },
            onSuccess: () => {
                errorText.hide();
                successText.show();
                testButton.setContent(getString('setHost.test'));
                screen.render();
            },
            onError: message => {
                successText.hide();
                switch(action){
                    case 'test': {
                        testButton.setContent(getString('setHost.test'));
                        errorText.setContent(getAnyString(
                            `setHost.${getString(`setHost.rawError.${message}`)}`,
                            `setHost.${message}`,
                            ['COMMAND_FAILED', { message }]
                        ));
                        break;
                    }
                    case 'submit': {
                        errorText.setContent(getString(`setHost.${message}`));
                        break;
                    }
                }
                errorText.show();
                screen.render();
                hostForm.focusChild({
                    'WRONG_CREDENTIALS': userInput,
                    'WRONG_DESTINATION': addressInput,
                    'ALREADY_EXISTS': nameInput
                }[getString(`setHost.rawError.${message}`, `setHost.${message}`)]);
            }
        });
    });
    screen.render();
    nameInput.focus();
};