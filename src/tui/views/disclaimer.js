import { getString } from '../../../strings.js';

import {
    form,
    button,
    text,
    flexContainer
} from '../components.js';

export default (
    screen
) => ({
    packageJson
}) => new Promise(resolve => {
    const disclaimerForm = form(screen);
    text(screen, { content: getString('disclaimer.title', { joinArrays: '\n\n', packageJson }) });
    const
        actions = flexContainer(disclaimerForm, { top: 6 }),
        cancelButton = button(actions, { content: getString('cancel') });
    cancelButton.on('press', () => resolve(false));
    button(actions, { content: getString('proceed') }, { color: 'green' })
        .on('press', () => disclaimerForm.submit());
    disclaimerForm.on('submit', () => resolve(true));
    screen.render();
    cancelButton.focus();
});