import {
    login as loginAccount,
    preregister as preregisterAccount,
    register as registerAccount
} from '@sshception/account';
import generateQr from '../../../lib/generateQr.js';
import packageJson from '../../../package.json' assert { type: 'json' };

import {
    loginTotp,
    disclaimer,
    register,
    enableTotp,
    pwnedPassword,
    invalidUsername,
    wrongPassword
} from '../views/main.js';

export default (
    screen
) => ({
    username,
    password,
    end
}) => new Promise(resolve => {
    let
        user,
        loginError,
        preregisterError;
    (async () => {
        try {
            user = await loginAccount(username, password)
        }
        catch(error){
            loginError = error;
        }
        if(user){
            const user = await loginAccount(username, password);
            if(user.isTotpLocked()){
                loginTotp(screen)({
                    isCancellable: false,
                    callback: ({
                        code,
                        onError
                    }) => {
                        if(user.unlockTotp(code))
                            resolve(user);
                        else
                            onError('invalidTotpCode');
                    }
                });
            }
            else
                resolve(user);
            return;
        }
        if(loginError){
            switch(loginError.message){
                case 'WRONG_USERNAME': {
                    if(!await disclaimer(screen)({ packageJson })) return end();
                    try {
                        await preregisterAccount(username, password);
                    }
                    catch(error){
                        preregisterError = error;
                    }
                    break;
                }
                case 'WRONG_PASSWORD': {
                    return wrongPassword(screen)({ end });
                }
            }
        }
        if(preregisterError){
            switch(preregisterError.message){
                case 'PWNED_PASSWORD': {
                    return pwnedPassword(screen)({ end });
                }
                case 'INVALID_USERNAME': {
                    return invalidUsername(screen)({ end });
                }
            }
        }
        register(screen)({
            username,
            callback: async ({
                repeatPassword,
                onError,
                isEnableTotp
            }) => {
                if(repeatPassword !== password) return onError('passwordMismatch');
                user = await registerAccount(username, password);
                if(isEnableTotp){
                    const {
                        url,
                        verify
                    } = await user.enableTotp();
                    enableTotp(screen)({
                        url,
                        qr: await generateQr(url),
                        callback: ({
                            isCancelled,
                            code,
                            onError
                        }) => {
                            if(isCancelled || verify(code))
                                return resolve(user);
                            else
                                onError('invalidTotpCode');
                        }
                    });
                }
                else
                    return resolve(user);
            }
        });
    })();
});