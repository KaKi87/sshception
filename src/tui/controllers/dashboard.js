import {
    PassThrough
} from 'node:stream';

import {
    chainedSession
} from '../../../lib/ssh.js';
import getHostChain from '../../../lib/getHostChain.js';
import generateQr from '../../../lib/generateQr.js';

import {
    home,
    setHost,
    loginTotp,
    enableTotp,
    editUserPass,
    deleteAccount
} from '../views/main.js';

import connectHost from '../../connectHost.js';

export default ({
    screen
}) => ({
    user,
    setActiveStream,
    activateScreen,
    end,
    emitter
}) => {
    const _home = () => home(screen)({
        hosts: user.getHosts(),
        callback: async ({
            action,
            host
        }) => {
            switch(action){
                case 'add-host':
                case 'edit-host': {
                    return setHost(screen)({
                        ...host,
                        callback: async ({
                            action: hostAction,
                            host: newHost,
                            onSuccess,
                            onError
                        }) => {
                            switch(hostAction){
                                case 'cancel': return _home();
                                case 'test': {
                                    const
                                        hosts = user.getHosts(),
                                        hostsChain = getHostChain(hosts, newHost);
                                    if(hostsChain.includes(undefined)) return onError('INVALID_CHAIN');
                                    try {
                                        await chainedSession({
                                            hostsChain,
                                            isTest: true
                                        });
                                        onSuccess();
                                    }
                                    catch(error){
                                        onError(error.level || error.message);
                                    }
                                    return;
                                }
                                case 'submit': {
                                    try {
                                        if(action === 'add-host')
                                            user.addHost(newHost);
                                        else if(action === 'edit-host')
                                            user.editHost(host.name, newHost);
                                        _home();
                                    }
                                    catch(error){
                                        onError(error.message);
                                    }
                                    return;
                                }
                            }
                        }
                    });
                }
                case 'remove-host': {
                    user.removeHost(host.name);
                    return _home();
                }
                case 'connect-host': {
                    try {
                        const
                            hosts = user.getHosts();
                        await connectHost({
                            hosts,
                            host,
                            activateScreen,
                            setActiveStream
                        });
                    }
                    catch(error){
                        _home();
                    }
                    return;
                }
                case 'toggle-totp': {
                    if(user.isTotpEnabled()) loginTotp(screen)({
                        isCancellable: true,
                        callback: ({
                            isCancelled,
                            code,
                            onError
                        }) => {
                            if(isCancelled) return _home();
                            try {
                                user.disableTotp(code);
                                _home();
                            }
                            catch(_){
                                onError('invalidTotpCode');
                            }
                        }
                    });
                    else {
                        const {
                            url,
                            verify
                        } = await user.enableTotp();
                        enableTotp(screen)({
                            url,
                            qr: await generateQr(url),
                            callback: ({
                                isCancelled,
                                code,
                                onError
                            }) => {
                                if(isCancelled || verify(code))
                                    _home();
                                else
                                    onError('invalidTotpCode');
                            }
                        });
                    }
                    return;
                }
                case 'edit-user-pass': {
                    return editUserPass(screen)({
                        username: user.getUsername(),
                        isTotpEnabled: user.isTotpEnabled(),
                        callback: async ({
                            isCancelled,
                            newUsername,
                            currentPassword,
                            newPassword,
                            repeatNewPassword,
                            totpCode,
                            onError
                        }) => {
                            if(isCancelled) return _home();
                            try {
                                if(newUsername)
                                    await user.setUsername(currentPassword, newUsername, totpCode);
                                if(newPassword){
                                    if(newPassword !== repeatNewPassword)
                                        onError('PASSWORD_MISMATCH');
                                    else
                                        await user.setPassword(currentPassword, newPassword, totpCode);
                                }
                                return _home();
                            }
                            catch(error){
                                onError(error.message);
                            }
                        }
                    });
                }
                case 'delete-account': {
                    return deleteAccount(screen)({
                        isTotpEnabled: user.isTotpEnabled(),
                        callback: async ({
                            isCancelled,
                            password,
                            totp,
                            onError
                        }) => {
                            if(isCancelled) return _home();
                            try {
                                await user.unregister(password, totp);
                                end();
                            }
                            catch(error){
                                onError(error.message);
                            }
                        }
                    });
                }
                case 'quit': end();
            }
        },
        isTotpEnabled: user.isTotpEnabled(),
        emitter
    });
    _home();
};