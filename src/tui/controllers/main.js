import auth from './auth.js';
import dashboard from './dashboard.js';

export default {
    auth,
    dashboard
};