import {
    getString,
} from '../../../strings.js';

import addHost from '../addHost.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('add-host')
    .description(getString('addHost'))
    .action(async () => {
        await addHost({
            screenStream,
            onAbort,
            user
        });
    });