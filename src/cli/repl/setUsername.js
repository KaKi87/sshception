import inquirerInput from '@inquirer/input';
import inquirerPassword from '@inquirer/password';
import chalk from 'chalk';

import { getString } from '../../../strings.js';
import createInquirerContext from '../../../lib/createInquirerContext.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('set-username')
    .description(getString('editUserPass.editUsername'))
    .argument('[username]', getString('editUserPass.newUsername'))
    .action(async newUsername => {
        if(!newUsername){
            newUsername = await inquirerInput(
                {
                    message: getString('editUserPass.newUsername')
                },
                createInquirerContext(screenStream, onAbort)
            );
        }
        let totpCode;
        const currentPassword = await inquirerPassword(
            {
                message: getString('editUserPass.currentPassword'),
                mask: '*'
            },
            createInquirerContext(screenStream, onAbort)
        );
        if(user.isTotpEnabled()){
            totpCode = await inquirerInput(
                {
                    message: getString('totpCode')
                },
                createInquirerContext(screenStream, onAbort)
            );
        }
        try {
            await user.setUsername(
                currentPassword,
                newUsername,
                totpCode
            );
            screenStream.stdin.write(`${chalk.green(getString('editUserPass.editUsernameSuccess'))}\n`);
        }
        catch(error){
            screenStream.stdin.write(`${chalk.red(getString(`editUserPass.${error.message}`, `$t(${error.message})`))}\n`);
        }
    });