import {
    getString
} from '../../../strings.js';

import listHosts from '../listHosts.js';
import addHost from '../addHost.js';
import editHost from '../editHost.js';
import removeHost from '../removeHost.js';
import connectHost from '../connectHost.js';

export default ({
    setActiveStream,
    onAbort,
    program,
    screenStream,
    user
}) => program
    .command('hosts')
    .alias('host')
    .description(getString('hostsCommand'))
    .argument('[name]', getString('setHost.name'))
    .option('-l, --list', getString('listHosts.title'))
    .option('-a, --add', getString('addHost'))
    .option('-e, --edit [name]', getString('editHost'))
    .option('-r, --remove [name]', getString('removeHost.title'))
    .option('-c, --connect [name]', getString('connectHost.title'))
    .action(async (
        oldName,
        {
            list: isList,
            add: isAdd,
            edit: isEdit,
            remove: isRemove,
            connect: isConnect
        }
    ) => {
        if(isList){
            listHosts({
                screenStream,
                user
            });
        }
        else if(isAdd){
            await addHost({
                screenStream,
                onAbort,
                user
            });
        }
        else if(isEdit){
            if(!oldName && typeof isEdit === 'string')
                oldName = isEdit;
            await editHost({
                screenStream,
                onAbort,
                user,
                oldName
            });
        }
        else if(isRemove){
            if(!oldName && typeof isRemove === 'string')
                oldName = isRemove;
            await removeHost({
                screenStream,
                onAbort,
                user,
                name: oldName
            });
        }
        else if(isConnect){
            if(!oldName && typeof isConnect === 'string')
                oldName = isConnect;
            await connectHost({
                setActiveStream,
                screenStream,
                user,
                name: oldName
            });
        }
    });