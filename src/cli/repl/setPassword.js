import inquirerInput from '@inquirer/input';
import inquirerPassword from '@inquirer/password';
import chalk from 'chalk';

import { getString } from '../../../strings.js';
import createInquirerContext from '../../../lib/createInquirerContext.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('set-password')
    .description(getString('editUserPass.editPassword'))
    .action(async () => {
        const newPassword = await inquirerPassword(
            {
                message: getString('editUserPass.newPassword'),
                mask: '*'
            },
            createInquirerContext(screenStream, onAbort)
        );
        await inquirerPassword(
            {
                message: getString('editUserPass.repeatNewPassword'),
                mask: '*',
                validate: newPasswordRepeat => newPassword === newPasswordRepeat || getString('editUserPass.PASSWORD_MISMATCH')
            },
            createInquirerContext(screenStream, onAbort)
        )
        let totpCode;
        const currentPassword = await inquirerPassword(
            {
                message: getString('editUserPass.currentPassword'),
                mask: '*'
            },
            createInquirerContext(screenStream, onAbort)
        );
        if(user.isTotpEnabled()){
            totpCode = await inquirerInput(
                {
                    message: getString('totpCode')
                },
                createInquirerContext(screenStream, onAbort)
            );
        }
        try {
            await user.setPassword(
                currentPassword,
                newPassword,
                totpCode
            );
            screenStream.stdin.write(`${chalk.green(getString('editUserPass.editPasswordSuccess'))}\n`);
        }
        catch(error){
            screenStream.stdin.write(`${chalk.red(getString(`editUserPass.${error.message}`, `$t(${error.message})`))}\n`);
        }
    });