import inquirerInput from '@inquirer/input';
import chalk from 'chalk';

import {
    getString
} from '../../strings.js';
import createInquirerContext from '../../lib/createInquirerContext.js';

import setHost from './setHost.js';
import testHost from './testHost.js';

export default async ({
    screenStream,
    onAbort,
    user,
    oldName
}) => {
    const hosts = user.getHosts();
    if(!oldName) oldName = await inquirerInput(
        {
            message: getString('setHost.name'),
            validate: _name => hosts.some(host => host.name === _name) || getString('setHost.NOT_EXISTS')
        },
        createInquirerContext(screenStream, onAbort)
    );
    const host = hosts.find(host => host.name === oldName);
    if(!host)
        return screenStream.stdin.write(`${chalk.red(getString('setHost.NOT_EXISTS'))}\n`);
    Object.assign(
        host,
        await setHost({
            screenStream,
            onAbort,
            hosts,
            host
        })
    );
    const isSuccess = await testHost({
        screenStream,
        onAbort,
        hosts,
        host
    });
    if(isSuccess){
        user.editHost(oldName, host);
        screenStream.stdin.write(`${chalk.green(getString('setHost.successEdit'))}\n`);
    }
};