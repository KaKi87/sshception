import inquirerInput from '@inquirer/input';
import inquirerPassword from '@inquirer/password';

import {
    isHostNameValid,
    isHostAddressValid,
    isHostPortValid,
    isHostUsernameValid,
    isHostPasswordValid
} from '@sshception/account';

import {
    getString
} from '../../strings.js';
import createInquirerContext from '../../lib/createInquirerContext.js';

export default async ({
    screenStream,
    onAbort,
    hosts,
    host = {}
}) => {
    host.name = await inquirerInput(
        {
            message: getString('setHost.name'),
            validate: _name => isHostNameValid(_name) || getString('setHost.INVALID_NAME')
        },
        createInquirerContext(screenStream, onAbort, host.name)
    );
    host.address = await inquirerInput(
        {
            message: getString('setHost.hostname'),
            validate: _name => isHostAddressValid(_name) || getString('setHost.INVALID_HOSTNAME')
        },
        createInquirerContext(screenStream, onAbort, host.address)
    );
    host.port = parseInt(await inquirerInput(
        {
            message: getString('setHost.port'),
            validate: _port => isHostPortValid(_port) || getString('setHost.INVALID_PORT')
        },
        createInquirerContext(screenStream, onAbort, host.port)
    ));
    host.username = await inquirerInput(
        {
            message: getString('setHost.username'),
            validate: _username => isHostUsernameValid(_username) || getString('setHost.INVALID_USERNAME')
        },
        createInquirerContext(screenStream, onAbort, host.username)
    );
    host.password = await inquirerPassword(
        {
            message: getString('password'),
            mask: '*',
            validate: _password => isHostPasswordValid(_password) || getString('setHost.INVALID_PASSWORD')
        },
        createInquirerContext(screenStream, onAbort, host.password)
    );
    host.chain = await inquirerInput(
        {
            message: getString('setHost.chain'),
            validate: _chain =>
                !_chain
                ||
                _chain.split(',').every(_name => hosts.some(({ name }) => _name === name))
                ||
                getString('setHost.INVALID_CHAIN')
        },
        createInquirerContext(screenStream, onAbort, host.chain)
    );
    return host;
};