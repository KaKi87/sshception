import inquirerInput from '@inquirer/input';
import chalk from 'chalk';

import {
    getString
} from '../../strings.js';
import createInquirerContext from '../../lib/createInquirerContext.js';

export default async ({
    screenStream,
    onAbort,
    user,
    name
}) => {
    screenStream.stdin.write(`${getString('removeHost.confirm')}\r\n`);
    await inquirerInput(
        {
            message: getString('setHost.name'),
            validate: _name => name === _name || getString('removeHost.confirmMismatch')
        },
        createInquirerContext(screenStream, onAbort)
    );
    let removeError;
    try {
        user.removeHost(name);
    }
    catch(error){
        removeError = error;
    }
    if(removeError)
        screenStream.stdin.write(`${chalk.red(getString(`removeHost.${removeError.message}`))}\n`);
    else
        screenStream.stdin.write(`${chalk.green(getString('removeHost.success'))}\n`);
};