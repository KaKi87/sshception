import inquirerInput from '@inquirer/input';
import chalk from 'chalk';

import generateQr from '../../lib/generateQr.js';
import { getString } from '../../strings.js';
import createInquirerContext from '../../lib/createInquirerContext.js';

export default async ({
    screenStream,
    onAbort,
    user
}) => {
    const {
        url,
        verify
    } = await user.enableTotp();
    screenStream.stdin.write(`${getString('enableTotp.url')} : ${url}\r\n${getString('enableTotp.qrCode')} :\r\n${(await generateQr(url)).replaceAll('\n', '\r\n')}\r\n`);
    await inquirerInput(
        {
            message: getString('totpCode'),
            validate: code => verify(code) || getString('invalidTotpCode')
        },
        createInquirerContext(screenStream, onAbort)
    );
    screenStream.stdin.write(`${chalk.green(getString('enableTotp.success'))}\r\n`);
};