# SSHception

![](https://shields.kaki87.net/uptimerobot/status/m790890781-fce7fd221b94d8ba97374fa6?label=ssh%20ssh.kaki87.net)
[![](https://shields.kaki87.net/badge/license-MIT-green)](./LICENSE.md)

This project is, at the same time, an SSH server, an SSH client, and a password manager.



| ![](./screenshot.png) | ![](./screenshot2.png) |
|-----------------------|------------------------|


## Getting started

### Installation

Requirements :
- `nodejs`
- `npm`
- `yarn`
- `ssh-keygen`

Start :
- Run `yarn keygen` to generate an SSH key pair
- Copy `config.example.js` as `config.js` and specify a custom port
- Run `yarn install` to install dependencies
- Run `yarn start` to start the SSH server

Stop : press `Ctrl + C`

### Testing

The account manager at [`@sshception/account`](/KaKi87/sshception-account) is unit-tested.

### Usage

- Connect using any SSH client
- Follow the steps

Automated TUI/CLI selection is based on terminal width.
Manual selection can be done using an `IS_TUI`/`IS_CLI` environment variable, e.g. `ssh -o "SetEnv IS_CLI=" ssh.kaki87.net`.

## FAQ

### Why should I use SSHception ?

#### It is open source.

This project is released under the [MIT license](LICENSE.md).

#### It is multiplatform.

Firstly, the server can run on any platform supporting NodeJS.

Secondly, the server can be accessed from any SSH client.

#### It respects your privacy.

Your data is encrypted using the AES-256 asymmetric encryption algorithm using your own as password.

The standard TOTP 2FA is supported.

### Why shouldn't I use SSHception ?

#### No direct connection.

Since this project is a server-side service, the SSH connection to your target host is not initiated from your client itself but from the SSHception server-side client then streamed to you.

Therefore, your SSH client-side logs will always log your connections to the SSHception server and your SSH server-side logs will always log the connections from the SSHception server.