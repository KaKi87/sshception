import i18next from 'i18next';

i18next.init({
    lng: 'en',
    resources: {
        'en': {
            translation: {
                'banner': '{{packageJson.name}} v{{packageJson.version}} ({{lastCommitId}}) | {{- packageJson.homepage}}\nRegister with a new password or login with an existing one',
                'password': 'Password',
                'totp': 'TOTP',
                'cancel': 'Cancel',
                'submit': 'Submit',
                'confirm': 'Confirm',
                'proceed': 'Proceed',
                'quit': 'Quit',
                'invalidTotpCode': 'Wrong TOTP code',
                'addHost': 'Add host',
                'totpCode': 'TOTP code',
                'editHost': 'Edit host',
                'COMMAND_FAILED': 'Command failed : {{- message}}',
                'COMMAND_ABORTED': 'Command aborted.',
                'WRONG_PASSWORD': 'Wrong password.',
                'TOTP_REQUIRED': 'TOTP code required.',
                'WRONG_TOTP': 'Wrong TOTP code.',
                'INVALID_USERNAME': 'Invalid username : only alphanumeric characters are allowed.',
                'PWNED_PASSWORD': 'This password has been breached : see https://haveibeenpwned.com/passwords for more details.',
                'deleteAccount': {
                    'title': 'Account deletion',
                    'confirm': 'I understand this cannot be undone.',
                    'confirmInvalid': 'You must confirm that you understand that this cannot be undone.',
                    'aborted': 'Account deletion aborted.'
                },
                'disclaimer': {
                    'title': [
                        '{{packageJson.name}} v{{packageJson.version}} | {{packageJson.description}}',
                        'By {{packageJson.author.name}} | {{- packageJson.homepage}}',
                        'Distributed under the {{packageJson.license}} license.'
                    ]
                },
                'editUserPass': {
                    'title': 'Edit username and/or password',
                    'currentUsername': 'Current username',
                    'newUsername': 'New username',
                    'currentPassword': 'Current password',
                    'newPassword': 'New password',
                    'repeatNewPassword': 'Repeat new password',
                    'editUsername': 'Edit username',
                    'editUsernameSuccess': 'Username successfully updated.',
                    'editPassword': 'Edit password',
                    'editPasswordSuccess': 'Password successfully updated.',
                    'ALREADY_EXISTS': 'Username already exists.',
                    'PASSWORD_MISMATCH': `Password confirmation doesn't match.`
                },
                'enableTotp': {
                    'title': 'TOTP setup',
                    'url': 'URL',
                    'qrCode': 'QR code',
                    'code': 'Input TOTP code to confirm',
                    'success': 'TOTP successfully enabled',
                    'alreadyEnabledError': 'TOTP already enabled.'
                },
                'home': {
                    'selectHost': 'Select host',
                    'disableTotp': 'Disable TOTP',
                    'enableTotp': 'Enable TOTP',
                    'editUserPass': 'Edit user/pass',
                    'deleteAccount': 'Delete account',
                    'host': 'Host',
                    'uri': 'URI',
                    'chain': '{{- host.name}} (via {{- via}})',
                    'chainSeparator': ' -> ',
                    'hostUri': '{{host.username}}@{{host.address}}:{{host.port}}',
                    'connect': 'Connect',
                    'edit': 'Edit',
                    'remove': 'Remove',
                    'selectedHost': 'Selected host',
                    'connecting': 'Connecting'
                },
                'loginTotp': {
                    'title': 'TOTP login'
                },
                'register': {
                    'title': 'Welcome {{username}}, please confirm your password to create your account.',
                    'enableTotp': 'Enable TOTP (next step)',
                    'passwordMismatch': 'This password does not match the one you entered via SSH.'
                },
                'listHosts': {
                    'title': 'List hosts',
                    'noHosts': 'No host configured yet.'
                },
                'setHost': {
                    'name': 'Name',
                    'nameNew': 'New name',
                    'address': 'Address',
                    'hostname': 'Hostname',
                    'port': 'Port',
                    'username': 'Username',
                    'setPassword': 'Set password',
                    'chain': 'Chain (comma-separated)',
                    'test': 'Test',
                    'testSuccess': 'SSH connection successful',
                    'testing': 'Testing',
                    'successAdd': 'Host successfully added.',
                    'successEdit': 'Host successfully edited.',
                    'WRONG_CREDENTIALS': 'wrong username or password',
                    'WRONG_DESTINATION': 'wrong address or port',
                    'INVALID_NAME': 'invalid name',
                    'INVALID_HOSTNAME': 'invalid hostname',
                    'INVALID_PORT': 'invalid port',
                    'INVALID_USERNAME': 'invalid username',
                    'INVALID_PASSWORD': 'invalid password',
                    'INVALID_CHAIN': 'invalid hosts chain',
                    'ALREADY_EXISTS': 'duplicate host name',
                    'NOT_EXISTS': 'host does not exist',
                    'rawError': {
                        'client-authentication': 'WRONG_CREDENTIALS',
                        'client-socket': 'WRONG_DESTINATION',
                        'client-timeout': 'WRONG_DESTINATION'
                    }
                },
                'disableTotp': {
                    'notEnabledError': 'TOTP is not enabled.',
                    'success': 'TOTP successfully disabled.'
                },
                'removeHost': {
                    'title': 'Remove host',
                    'confirm': 'Re-enter the host name to confirm its deletion',
                    'confirmMismatch': 'Name confirmation does not match',
                    'success': 'Host successfully removed.',
                    'WRONG_NAME': 'No host exists with this name.'
                },
                'connectHost': {
                    'title': 'Connect to host',
                    'connecting': 'Connecting...'
                },
                'hostsCommand': 'List/add/edit/remove/connect hosts'
            }
        }
    }
});

export const getString = i18next.t;

export const getAnyString = (...items) => {
    for(const item of items){
        if(i18next.exists(...Array.isArray(item) ? item : [item]))
            return i18next.t(...Array.isArray(item) ? item : [item]);
    }
};